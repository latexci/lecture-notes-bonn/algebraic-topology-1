\lecture[]{Fr 13 Okt 2023}{Untitled}

\begin{tikzpicture}
  \foreach \x in {0,..., 4} {
    \foreach \y in {0,...,4} {
      \node (E\x\y) at (\x, \y) {$E_{\x, \y}^2$};
    }
  }
\end{tikzpicture}
\todo{finish figure}


\begin{definition}
  \label{def:first-quadrant-spectral-sequence}
  We say that a spectral sequence is \vocab{first quadrant},
  if all abelian groups $E^2_{p,q}$ are trivial whenever $p < 0$ or $q < 0$.
\end{definition}

\begin{lemma}
  \label{lm:stabilizing-of-first-quadrant-spectral-sequences}
  For a first quadrant spectral sequence $(E^{\chainbullet}, d^\chainbullet, h^\chainbullet)$,
  we have $E_{p,q}^r = 0$ if $p < 0$, $q < 0$ for all $r$.

  Moreover, for a given $(p,q) \in \mathbb{Z}^2$, the map $h$ induces an isomorphism
  $E^r_{p,q} \xrightarrow{\cong} E^{r+1}_{p,q}$ whenever $r > r_0 = \max (p, q+1)$,
  i.e.~the groups $E_{p,q}^r$ stabilize as $r \to \infty$.
\end{lemma}

\begin{proof}
  The first statement follows directly from the existence of the isomorphisms
  $h^{\chainbullet}$ by induction on $r$.

  For the second statement, if $r > r_0$, then the target of the differential
  $d^r \colon E_{p,q}^{r} \to E_{p-r, p + r - 1}^r$ is trivial (since $p - r < 0$),
  hence every element in $E_{p,q}^r$ is a cycle.

  Moreover, the incoming differential $d^r\colon E_{p + r, q - r + 1}^r \to E_{p,q}^r$
  is trivial, since $p - r + 1 < 0$.
  Hence
  \begin{tikzcd}
    E_{p,q}^r \ar{r}{\cong}
    &
    H_*(E_{p,q}^r)
    \ar{r}{h^r}[swap]{\cong}
    &
    E_{p,q}^{r+1}
  \end{tikzcd}
  is an isomorphism as desired.
\end{proof}

\begin{definition}
  \label{def:infinity-page-of-first-quadrant-spectral-sequence}
  For a first quadrant spectral sequence $(E^\chainbullet, d^\chainbullet, h^\chainbullet)$,
  we define the \vocab{$E^{\infty}$-page} as the bigraded abelian group
  \[
    E_{p,q}^{\infty} = E_{p,q}^{r_0(p,q) + 1}
  \]
  with $r_0(p,q) \coloneqq \max (p, q+1)$.
\end{definition}

By \autoref{lm:stabilizing-of-first-quadrant-spectral-sequences},
$E_{p,q}^{\infty} \cong E_{p,q}^r$ for all $r > r_0(p,q)$.

\begin{definition}+
  \label{def:filtered-object-in-abelian-category}
  By a filtered object in an abelian category $\mathcat{A}$,
  we mean an object $H \in \mathcal{A}$ with a sequence of inclusions
  \[
    0 = F^{-1} \subset F^0 \subset F^1 \subset \dotsb \subset F^n \subset \dotsb \subset H
    .
  \]
\end{definition}

We will apply this to the case where $\mathcal{A}$ is the category of graded
abelian groups and $H = H_*(E, \mathbb{Z})$.

\begin{definition}
  \label{def:convergence-of-spectral-sequence}
  A first quadrant spectral sequence $(E^\chainbullet, d^\chainbullet, h^\chainbullet)$
  is said to \vocab{converge} to a filtered object in graded abelian groups
$(H, F = \set{ F^n_m }_{n \in \mathbb{N}\cup \set{ -1 }}^{m \in \mathbb{Z} } )$
  if there is a chosen isomorphism
  \[
    E^{\infty}_{p,q} \cong \faktor{F_{p+q}^p}{F_{p+q}^{p-1}}
  \]
  for all $p$, $q$ and $F_n^p = H$ if $n \geq p$.

  In this case, we write $E_{p,q}^2 \implies H$.
\end{definition}

\[
  \begin{tikzpicture}
    \draw[->] (0,0) -- (4,0);
    \draw[->] (0,0) -- (0,4);
  \end{tikzpicture}
\]


\begin{remark}
  Note that for any first quadrant spectral sequence,
  there is some $(H,F)$ that it converges to.
\end{remark}

\begin{remark}
  Convergence is really a \vocab{datum} of the isomorphism
  $E_{p,q}^{\infty} \cong F_{p+q}^p / F_{p+q}^{p-1}$
  and not a property of.

  Convergent spectral sequences are often simply encoded as $E^2_{p,q} \implies H$,
  but this suppresses not only this data above, but also the higher pages,
  the differentials and the filtration on $H$.
\end{remark}

We now want to introduce the Serre spectral sequence
for the homology of fibre sequences.
We now recall exactly what we mean by fibre sequences.

\begin{definition}
  \label{def:homotopy-fibre}
  Let $f\colon Y \to X$ be a continuous map of topological spaces
  and let $x\in X$ be a point.
  The \vocab{homotopy fibre} $\hofib_x(f)$ is defined to be the space
  \[
    \hofib_x(f) \coloneqq P_xX \times _X Y
    ,
  \]
  where
  \[
    P_x X \coloneqq \set{ γ \colon [0,1] \to X \suchthat γ(1) = x } 
  \]
  is the \vocab{based path space} of $X$ at $x$ that comes with a map
  \[
    P_x X \xrightarrow{\ev_0} X
  \]
  given by the evaluation at $0$.
  In other words, there is a pullback square
  \[
    \begin{tikzcd}
      \hofib_x(F)
      \ar[swap]{d}{}
      \ar{r}{}
      \pullback
      &
      P_x X
      \ar{d}{\ev_0}
      \\
      Y
      \ar[swap]{r}{f}
      &
      X
    \end{tikzcd}
    .
  \]
  In words, $\hofib_x(f)$ is the space of pairs $(γ,y)$,
  where $y\in Y$ and $γ$ is a path in $X$ from $f(y)$ to $x$.
\end{definition}

\begin{remark}
  Note that $P_x X$ is contractible by the homotopy
    \begin{equation*}
    H: \left| \begin{array}{c c l} 
      P_xX \times [0,1] & \longrightarrow & P_xX \\
      (γ,t) & \longmapsto &    \left| \begin{array}{c c l} 
        [0,1] & \longrightarrow & X \\
        s & \longmapsto	 &  γ((1-t)s + t)
        .
      \end{array} \right.
    \end{array} \right.
  \end{equation*}
\end{remark}

\begin{example}
  Consider $\star \xrightarrow{f} X$ with $\star \mapsto x$.
  Then $\hofib_x(f) = \Omega_x X$ is the loop space.
\end{example}

\begin{definition}
  A \vocab{fibre sequence} of topological spaces is
  a sequence $F \xrightarrow{ι} Y \xrightarrow{f} X$,
  a basepoint $x \in X$,
  a homotopy $h\colon F \to X^{[0,1]}$ from the composite $f \circ ι$
  to the constant map $c_x\colon F \to X$ and such that the induced map
    \begin{equation*}
    \begin{array}{c c l} 
    F & \longrightarrow & \hofib_x(f) \\
    z & \longmapsto &  (h(z), ι(z))
    \end{array}
  \end{equation*}
  is a weak homotopy equivalence.
\end{definition}

\begin{recall}
  A weak homotopy equivalence is a map that induces isomorphisms
  on $\pi_n(-, x)$ for all $n\in \mathbb{N}$ and all basepoints $x$.
\end{recall}

\begin{example}
  \label{ex:fibre-sequences-from-homotopy-fibres}
  \begin{enumerate}[1)]
    \item 
  Let $f\colon Y \to X$ be any continuous map and $x\in X$.
  Then the pair
  \[
    (\hofib_x f \xrightarrow{i}
    Y \xrightarrow{f}
    X,
    h = γ)
  \]
  is a fibre sequence,
  since by construction the map $\hofib_x(f) \to \hofib_x(f)$ is the identity.

  Every fiber sequence is equivalent to such an example in the following sense:
  Given a fibre sequence $(F \xrightarrow{i}  Y \xrightarrow{f} X, h)$,
  there is a commutative diagram
  \[
    \begin{tikzcd}
      F
      \ar{d}[swap]{i}
      \ar{r}{\simeq}
      &
      \hofib_x(f)
      \ar{d}
      \\
      Y
      \ar{r}{\id}
      \ar{d}[swap]{f}
      &
      Y
      \ar{d}{f}
      \\
      X
      \ar{r}{\id}
      &
      X
    \end{tikzcd}
  \]
  which is an equivalence of fibre sequences.

  In particular,
  \[
    \Omega X \to  \star \to X
  \]
  is a fibre sequence, where $h\colon \Omega X \times [0,1] \to X$ is the evaluation map.

  \begin{warning}
    If one instead chooses the constant homotopy,
    one does not obtain a fibre sequence.

    This is because in this case, the induced map $\Omega X \to \hofib_x(f) = \Omega X$
    is the constant map, which is not a weak homotopy equivalence
    (unless $X$ is weakly contractible).

    Hence, the choice of homotopy is important.
  \end{warning}

  \item

  For every pair of spaces $F$ and $X$ with $x\in x$,
  the pair $(F \to F \times X \xrightarrow{\pr_X} X, h = \const)$
  is a fibre sequence, called the \vocab{trivial fibre sequence}.

  To see that, note that
  \[
    \hofib_x(\pr_X) = F \times P_x X
  \]
  with induced map
    \begin{equation*}
    \begin{array}{c c l} 
    F & \longrightarrow & F \times P_x X \\
    y & \longmapsto &  (y, \const)
    ,
    \end{array}
  \end{equation*}
  a homotopy equivalence as $P_x X$ is contractible.

  \item

  Let $p \colon E \to B$ be a fibre bundle with fibre $F = p^{-1} (h)$
  for some $b\in B$.
  Then the sequence $(F \to E \xrightarrow{p} B$) with the constant homotopy
  is a fibre sequence.
  This is a special case of the next example.
\item Recall that $p \colon E \to B$ is a \vocab{Serre fibration}
  if in every diagram of the form
  \[
    \begin{tikzcd}
      D^n \times \set{ 0 } 
      \ar[swap]{d}{}
      \ar{r}{}
      &
      E
      \ar{d}{p}
      \\
      D^n \times I
      \ar[swap]{r}{}
      \ar[dashed]{ur}
      &
      B
      ,
    \end{tikzcd}
  \]
  there exists a lift $D^n \times I \to E$ making both triangles commute.

  Given a Serre fibration $p \colon E \to B$ and any point $b\in B$,
  the sequence $(F \coloneqq p^{-1} (b) \to E \to B)$ with the constant
  homotopy is a fibre sequence.

  \begin{proof}
    Exercise sheet 1.
  \end{proof}
  \begin{note}
    Every fibre sequence is also equivalent to one of this form.
  \end{note}
\item As a special case of $3)$, the \vocab{Hopf fibration}
  is a fibre bundle $S^1 \to S^3 \xrightarrow{\eta} S^2$.
  It arises by letting $S^1 = U(1)$ act on $S^3 \subset \mathbb{C}^2$
  via $(λ \cdot (x_1, x_2) = (λx_1, λx_2)$,
  with quotient space $\mathbb{C}\mathbb{P}^1 \cong S^2$.
\item Example $5)$ generalizes to fibre bundles
  $S^1 \to S^{2n+1} \to \mathbb{C}\mathbb{P}^n$
  with limit case
  \[
    \begin{tikzcd}
      S^1
      \ar{r}
      \ar{d}[swap]{\simeq}
      &
      S^{\infty}
      \ar{r}
      \ar{d}[swap]{\simeq}
      &
      \mathbb{C}\mathbb{P}^{\infty}
      \ar{d}{=}
      \\
      \Omega \mathbb{C}\mathbb{P}^{\infty}
      \ar{r}
      &
      \star
      \ar{r}
      &
      \mathbb{C}\mathbb{P}^{\infty}
    \end{tikzcd}
  \]
  \end{enumerate}
\end{example}
