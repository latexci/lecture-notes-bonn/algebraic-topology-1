\lecture[Revision of some facts about $\pi_k(S^n)$. Computing homotopy groups using homotopy fibers. Motivation for spectral sequences.]{Mo 9 Okt 2023}{Introduction}


\section{Informal introduction}

In this semester, we will study:
\begin{itemize}
  \item The Serre spectral sequence
  \item Characteristic classes
  \item Bordism
\end{itemize}

One of the big goals of homotopy theory is to compute
\[
  [X,Y]_* \coloneqq \faktor{\set{ \text{basepoint preserving cont.~maps $X \to Y$} } }{\text{basepoint preserving homotopy}}
\]
for pointed CW-complexes $X$ and $Y$.

CW-complexes are built out of spheres, hence the building blocks for these sets
\[
  [S^k, S^n]_* = \pi_k(S^n)
  .
\]
For $n \geq  1$, these are groups, and even abelian for $n \geq 2$.

\begin{question}
  What do we know about the homotopy groups $\pi_k(S^n)$?
\end{question}

\begin{itemize}
  \item $\pi_k(S^n, \star) = 0$  whenever $k < n$.
    This can be proved by cellular approximation.
  \item $\pi_n(S^n, \star) \cong \mathbb{Z}$ by the Hurewicz theorem and
    $H_n(S^n, \mathbb{Z}) = \mathbb{Z}$.
  \item $\pi_k(S^1, \star) = 0$ for $k > 2$.
    This can be proved using covering space theory,
    since $\mathbb{R} \xrightarrow{\exp }
    S^1 \subset \mathbb{C}$ is a universal cover.
  \item $\pi_3(S^2, \star) \neq 0$.
    To see this, consider the attaching map of the $4$-cell
    of $\mathbb{C}\mathbb{P}^2$ and denote it
    $\eta\colon S^3 \to S^2 \cong \mathbb{C}\mathbb{P}^1$.
    If this were nullhomotopic, then
    $\mathbb{C}\mathbb{P}^2 \simeq S^2 \wedge S^4$,
    which contradicts the ring structure on
    \[
      H^*(\mathbb{C}\mathbb{P}^2) \cong \mathbb{Z}[X] / X^3
    \]
  \item The sequence
    \[
      \pi_k(S^n, \star) \to \pi_{k+1}(S^{n+1}, \star) \to \pi_{k+2}(S^{n+2},\star)
    \]
    eventually stabilizes. This is called the Freudenthal suspension theorem.
\end{itemize}

To go beyond these observations, we need a new tool, the \vocab{Serre spectral sequence}. 
To motivate why it is useful for this question, consider the following strategy:

There exists a map $f\colon S^2 \to K(\mathbb{Z}, 2)$ into an Eilenberg-MacLane space
which induces an isomorphism
\[
  f_*\colon \pi_2(S^2, \star) \xrightarrow{\cong} p_2(K(\mathbb{Z}, 2))
  .
\]
We can take the homotopy fibre $H = \hofib_*(f)$.
There is a fibre sequence $H \to S^2 \xrightarrow{f} K(\mathbb{Z},2)$,
hence we get a long exact sequence of homotopy groups
\[
  \begin{tikzcd}[column sep = tiny]
  &
  \pi_4(S^2, \star)
  \connectingmark[Y]
  \ar[red]{r}{\cong}
  &
  \underbrace{\pi_{4} ( K(\mathbb{Z},2), \star )}_{=0}
  \connectingmap[Y]{\partial}
  \\
  \pi_3 ( H, \star )
  \ar[red]{r}{\cong}
  &
  \pi_3 ( S^2, \star )
  \connectingmark[Z]
  \ar{r}
  &
  \underbrace{\pi_3 ( K(\mathbb{Z},2), \star )}_{=0}
  \connectingmap[Z]{\partial}
  \\
  \underbrace{\pi_2 ( H, \star )}_{\color{red} = 0}
  \ar{r}
  &
  \underbrace{\pi_2(S^2, \star)}_{=\mathbb{Z}}
  \ar{r}{\cong}
  &
  \underbrace{\pi_2(K(\mathbb{Z},2), \star)}_{=\mathbb{Z}}
  \end{tikzcd}
\]

Hence, we deduce that $H$ is $2$-connected and $\pi_n(H, \star) \xrightarrow{\cong}
\pi_n(S^2, \star)$ for all $n \geq 3$.

Therefore, by the Hurewicz theorem, we deduce that
\[
  \begin{tikzcd}[column sep = tiny]
  \pi_3(H, \star) \ar{rr}{\cong} \ar[swap]{dr}{\cong} & & H_3(H, \mathbb{Z}) \ar{dl}{\cong} \\
  & \pi_3(S^2, \star)
  \end{tikzcd}
\]
are all isomorphisms.
If we had a way to compute $H_*(H, \star)$ from $H_*(S^2, \mathbb{Z})$,
and $H_*(K(\mathbb{Z},2), \mathbb{Z})$ (both of which are known),
then we can compute $\pi_3(S^2)$ in this way.

Upshot: It would be useful to have a tool that which relates
the homology groups of the three terms in a fibre sequences.
This also helps to compute $\pi_k(S^n, \star)$ in other ways
(for example, we will show that $\pi_n(S^k, \star)$ is finite unless
$n = k$ or $n = 2k-1$ and $k$ even).

Furthermore, the Serre spectral sequence will allow us to compute the
(co)homology of spaces like $U(n)$, $SU(n)$, $\Omega S^n$, $K(\mathbb{Z}/2, n)$
and (re)prove structural theorems like the Hurewicz theorem,
the Freudenthal suspension theorem and Thom isomorphisms.

\begin{question}
  So, given a fibre sequence $F \to  Y \to X$,
  what could the relationship between the homology groups of $F$, $Y$ and $X$ be?
\end{question}

\begin{example}
  Consider the easy case of the trivial fibration
  $F \to  F \times X \xrightarrow{\pr_X} X$.

  Then, the Alexander-Whitney map induces an isomorphism
  \[
    H_n(X \times F, \mathbb{Z}) \to \directsum_{p + q = n} H_p(X, H_q(F))
    .
  \]
  This computes the homology of the \enquote{total space}
  in terms of the homology of $X$ and $F$.
\end{example}

\begin{example}[Hopf fibration]
  Consider again the \vocab{Hopf fibration} $S^1 \to S^3 \xrightarrow{\eta} S^2$.

  \begin{center}
    \begin{tabular}{c | c | c}
      $n $ & $H_n(S^3, \mathbb{Z})$ & $\directsum_{p+q = n} H_p(S^2, H_q(S^1, \mathbb{Z}))$
      \\
      0 & $\mathbb{Z}$ & $\mathbb{Z}$ \\
      1 & 0 & $\mathbb{Z}$ \\
      2 & 0 & $\mathbb{Z}$ \\
      3 & $\mathbb{Z}$ & $\mathbb{Z}$
    \end{tabular}
  \end{center}

  Thus, the direct sum is \enquote{too big} to describe $H_n(S^3, \mathbb{Z})$
  in degree $n = 1$, $2$.

  Note, however, that we consider the $2$-step filtration $S^1 \subset S^3$
  which satisfies
  \[
    \tilde{H}_*\left(\faktor{S^3}{S^1}, \mathbb{Z}\right) \cong
    \begin{cases}
      \mathbb{Z} & n = 2, 3 \\
      0 & \text{else}
    \end{cases}
    .
  \]

  Hence, adding this reduced homology to $H_n(S^3, \mathbb{Z})$ makes the table
  match again.

  This does not agree with $H_*(S^3, \mathbb{Z})$ because the connecting
  homomorphism in the long exact sequence
  \[a
    H_3(S^2, \mathbb{Z}) \to \tilde{H}_3(S^3 / S^1, \mathbb{Z}) \xrightarrow{\partial }
    H_1(S^1, \mathbb{Z}) \to H_1(S^3, \mathbb{Z})
  \]
  is an isomorphism so that $H_2(S^3 / S^1, \mathbb{Z})$ 
  does not contribute to $H_1(S^3, \mathbb{Z})$ and $H_1(S^1, \mathbb{Z})$
  does not contribute to $H_1(S^e, \mathbb{Z})$.
\end{example}

It turns out that something similar holds for all fibre sequences $F \to Y \to X$:
There always exists a filtration
\[
  F_0 \subset F_1 \subset \dotsb \subset F_m \subset \dotsb \subset C_*(Y,\mathbb{Z})
\]
on $C_*(Y, \mathbb{Z})$ such that
\[
  H_{p+q} (F_p / F_{p-1}) \cong C_p^{\cell}(X, H_q(F, \mathbb{Z}))
  .
\]

To understand $H_*(Y, \mathbb{Z})$, one needs to understand the cancellations in the
associated long exact sequence.

This is best encoded in a \vocab{spectra sequence}. 


\section{Spectra sequences}

\begin{definition}
  A (homologically, Serre-graded) \vocab{spectral sequence} is a triple
  $(E^\chainbullet, d^\chainbullet, h^\chainbullet)$,
  where
  \begin{itemize}
    \item $(E^r)_{r \geq 2}$ is a sequence of $\mathbb{Z}$-bigraded abelian groups.
      We write $E^r_{p,q}$ for the corresponding direct summands.
      $E^r$ is called the $r$-th \vocab{page} of the spectral sequence. 
    \item $d^r\colon  E^r \to E^r$ is a sequence of morphisms
      (called differentials) of bidegree $(-r, r-1)$
      satisfying $d^r \circ d^r = 0$.
    \item $h^r\colon H_*(E^r) \to E^{r+1}$ is a sequence
      of bigrading preserving isomorphisms.
      Here, $H_*^r(E^r)$ denotes the homology with respect to $d^r$,
      inheriting the bigrading from $E^r$.
  \end{itemize}
\end{definition}

\begin{remark}
  Depending on where you look, you might also find definitions where $r$
  starts at $1$ or $0$, but we will use $2$ for this course.
\end{remark}
