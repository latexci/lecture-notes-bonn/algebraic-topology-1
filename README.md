# Algebraic Topology 

These are the lecture notes for the 'Algebraic Topology', taught in winter term 23/24 at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://latexci.gitlab.io/lecture-notes-bonn/algebraic-topology-1/2023_Algebraic_Topology.pdf
[2]: https://latexci.gitlab.io/lecture-notes-bonn/algebraic-topology-1/2023_Algebraic_Topology.log
[3]: https://latexci.gitlab.io/lecture-notes-bonn/algebraic-topology-1/
